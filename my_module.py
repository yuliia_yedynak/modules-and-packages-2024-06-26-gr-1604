s = "If Comrade Napoleon says it, it must be right."
a = [100, 200, 300]

print(f"a: {a}")
print(f"s: {s}")
print(__name__)

def foo(arg: str):
    return f"arg: {arg}"


class Foo:
    pass


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        print(foo(sys.argv[1]))
    print("Module started as main program.")
    arg = "my_string"
    assert arg in foo(arg), "Error!"
    print(f"a: {a}")
    print(f"s: {s}")
    print(__name__)

